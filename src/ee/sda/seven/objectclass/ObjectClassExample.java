package ee.sda.seven.objectclass;

import ee.sda.seven.objectandclasses.Investment;

public class ObjectClassExample {

    public static void main(String[] args) {
        // Java object class
        // It is the first class in Java
        Object variableName = new Object();

        Investment investment = new Investment();

    }
}

package ee.sda.seven.array;

// Longest common prefix

/* Write a function to find the longest common prefix string amongst an array of strings.

        If there is no common prefix, return an empty string "".

        Example 1:

        Input: strs = ["flower","flow","flight"]
        Output: "fl"
        Example 2:

        Input: strs = ["dog","racecar","car"]
        Output: ""
        Explanation: There is no common prefix among the input strings.*/

// words = ['cooperative', 'coworking', 'cooking', 'cooling']

import java.util.Scanner;

public class LCPTask {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        // cooperative coworking cooking cooling
        System.out.println("Please enter a set of strings: ");

        String line = scanner.nextLine();

        // Convert line into array of string
        String[] arrayOfStrings = line.split(" ");

        // Result will be here
        String lcp = "";

        boolean stopOuterLoop = false;
        // Length of shortest string is 6 because cooling
        for (int i = 0; i < 6; i++) {
            // Current char from the first string
            String firstString = arrayOfStrings[0];
            char currentChar = firstString.charAt(i);

            for (int j = 0; j < arrayOfStrings.length; j++) {
                String secondString = arrayOfStrings[j];
                char secondChar = secondString.charAt(i);
                if(secondChar != currentChar){
                    stopOuterLoop = true;
                    break;
                }
            }
            if(stopOuterLoop){
                break;
            }
            lcp = lcp + currentChar;
        }

        System.out.println("The longest common prefix is : " +lcp);
    }
}

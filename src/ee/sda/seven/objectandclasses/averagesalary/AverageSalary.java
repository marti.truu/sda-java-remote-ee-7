package ee.sda.seven.objectandclasses.averagesalary;

public class AverageSalary {


    public static void main(String[] args) {

        Employee employee1 = new Employee("John", "Doe", 2000);
        Employee employee2 = new Employee("Kate", "Doe", 2500);
        Employee employee3 = new Employee("Richard", "Doe", 2100);
        Employee employee4 = new Employee("Mark", "Doe", 2600);

        Employee[] employees = {employee1, employee2, employee3, employee4};

        double result = calculateAverageSalary(employees);

       System.out.println("Your employees average salary is: " + result);
    }

    public static double calculateAverageSalary(Employee[] employees){

        // Average salary of employees = sum of employees salaries/number of employees

        double avgSalary = 0;

        double sum = 0;

        for (int i = 0; i < employees.length; i++) {
            sum = sum + employees[i].getSalary();
        }

        avgSalary = sum/employees.length;

        return avgSalary;
    }
}

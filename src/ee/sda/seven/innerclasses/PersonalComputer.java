package ee.sda.seven.innerclasses;

public class PersonalComputer {

    private double price;

    class CPU {
        double cores;
        String manufacturer;
        //...
    }

    class RAM {
        int memory;
        double clockSpeed;
        //...
    }

    class SSD {
        double memorySize;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    //...

    // What we need to build a computer
    // Board
    // Hard Drive, HDD or SSD
    // RAM - memory, clock speed, manufacturer
    // CPU
    // WiFi card
    // GPU card

    //TODO: Homework 1:
    // Find any real world Object which has some parts and try to build it with
    // Inner classes as we did with PC.
}
